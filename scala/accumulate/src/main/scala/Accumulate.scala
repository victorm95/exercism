class Accumulate {
  def accumulate[A, B](fn: A => B, data: List[A]): List[B] =
    if (data.isEmpty) Nil
    else fn(data.head) :: accumulate(fn, data.tail)
}
