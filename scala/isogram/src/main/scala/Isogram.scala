object Isogram {
  def isIsogram(text: String): Boolean = {
    val filtered = text.toLowerCase.filter(_.isLetter)
    filtered.forall(char => filtered.count(_ == char) == 1)
  }
}
