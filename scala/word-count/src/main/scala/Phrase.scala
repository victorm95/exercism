class Phrase(text: String) {
  val words = text.toLowerCase.split("""[^\w']+""")

  def wordCount: Map[String, Int] =
    this.words.map(word => word -> this.words.count(_ == word)).toMap
}
