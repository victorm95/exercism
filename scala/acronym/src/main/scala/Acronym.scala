object Acronym {
  def getUpper(word: String): String =
    if (word forall (_.isUpper)) word.head.toString
    else if (word forall (_.isLower)) word.head.toUpper.toString
    else word filter (_.isUpper)

  def abbreviate(phrase: String): String =
    phrase.split("""[^\w]+""")
      .map(getUpper)
      .mkString
}
