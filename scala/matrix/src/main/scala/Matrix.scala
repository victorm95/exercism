case class Matrix(text: String) {
  val rows = text.split("\n")
    .map(
      _.split(" ").map(_.toInt).toVector
    )
    .toVector

  def cols(index: Int): Vector[Int] =
    rows.map(_.apply(index))
}
