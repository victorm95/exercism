import scala.collection.mutable.HashSet
import scala.util.Random

class Robot {
  private var _name = Robot.genName

  def name: String =
    _name

  def reset(): Unit =
    _name = Robot.genName
}

object Robot {
  private val chars = 'A' to 'Z'
  private val store = new HashSet[String]

  private def genRandomNumbers(amount: Int): String = {
    val number = Random.nextInt(10).toString
    if (amount > 1) number + genRandomNumbers(amount - 1)
    else number
  }

  private def genRandomChars(amount: Int): String = {
    val char = chars(Random.nextInt(chars.size))
    if (amount > 1) char + genRandomChars(amount - 1)
    else char.toString
  }

  def genName: String = {
    val name = genRandomChars(2) + genRandomNumbers(3)
    if (!store.contains(name)) {
      store += name
      name
    }
    else genName
  }
}
