object CollatzConjecture {
  private def calc(x: Int, step: Int): Int =
    if (x == 1) step
    else if (x % 2 == 0) calc(x / 2, step + 1)
    else calc(x * 3 + 1, step + 1)

  def steps(x: Int): Option[Int] =
    if(x > 0) Some(calc(x, 0))
    else None
}
