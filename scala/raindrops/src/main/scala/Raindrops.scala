object Raindrops {
  def convert(n: Int): String = {
    val result = (if (n % 3 == 0) "Pling" else "") +
      (if (n % 5 == 0) "Plang" else "") +
      (if (n % 7 == 0) "Plong" else "")

    if (result.isEmpty) n.toString
    else result
  }
}
