object Scrabble {
  private val scores = List(
    List('A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T') map (_ -> 1),
    List('D', 'G') map (_ -> 2),
    List('B', 'C', 'M', 'P') map (_ -> 3),
    List('F', 'H', 'V', 'W', 'Y') map (_ -> 4),
    List('K') map (_ -> 5),
    List('J', 'X') map (_ -> 8),
    List('Q', 'Z') map (_ -> 10)
  ).flatten.toMap

  def scoreLetter(letter: Char): Int =
    scores.getOrElse(letter.toUpper, 0)

  def scoreWord(word: String): Int =
    word.map(scoreLetter).sum
}
