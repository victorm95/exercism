object SumOfMultiples {
  def multiples(limit: Int)(num: Int): Seq[Int] =
    (1 until limit) filter (_ % num == 0)

  def sumOfMultiples(factors: Set[Int], limit: Int): Int =
    factors.flatMap(this.multiples(limit)).sum
}
