import scala.collection.mutable.{Map => MutMap}

class School {
  type DB = Map[Int, Seq[String]]

  private val internalDb: MutMap[Int, List[String]] = MutMap()

  def add(name: String, g: Int) = {
    val names = grade(g).toList
    internalDb.put(g, names ::: name :: Nil)
  }

  def db: DB =
    internalDb.toMap

  def grade(g: Int): Seq[String] =
    internalDb.getOrElse(g, Nil)

  def sorted: DB =
    internalDb
      .toList
      .sortBy(_._1)
      .map{ case (grade, names) => (grade, names.sorted) }
      .toMap
}
