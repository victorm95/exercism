object BeerSong {
  private def bottles(x: Int): String =
    if (x == 0) "no more bottles"
    else if (x == 1) "1 bottle"
    else s"$x bottles"

  private def take(x: Int): String =
    if (x < 1) "Go to the store and buy some more, 99 bottles of beer on the wall."
    else if (x == 1) "Take it down and pass it around, no more bottles of beer on the wall."
    else s"Take one down and pass it around, ${bottles(x - 1)} of beer on the wall."

  def verse(x: Int): String =
    s"${bottles(x).capitalize} of beer on the wall, ${bottles(x)} of beer.\n${take(x)}\n"

  def verses(start: Int, finish: Int): String =
    (start to finish by -1).map(verse).mkString("\n")
}
