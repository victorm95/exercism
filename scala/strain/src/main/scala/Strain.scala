object Strain {
  def keep[T](list: Seq[T], fn: T => Boolean): Seq[T] =
    if (list.isEmpty) Nil
    else if (fn(list.head)) list.head +: keep(list.tail, fn)
    else keep(list.tail, fn)

  def discard[T](list: Seq[T], fn: T => Boolean): Seq[T] = {
    val negated: T => Boolean = el => !fn(el)
    keep(list, negated)
  }
}
