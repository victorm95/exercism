class Clock (val hours: Int, val minutes: Int) {
  def +(that: Clock): Clock = Clock(hours + that.hours, minutes + that.minutes)

  def -(that: Clock): Clock = Clock(hours - that.hours, minutes - that.minutes)

  override def equals(that: Any): Boolean = that match {
    case Clock(h, m) => hours == h && minutes == m
    case _ => false
  }

  override def toString(): String = s"Clock($hours, $minutes)"
}

object Clock {
  def apply(m: Int): Clock = Clock(0, m)

  def apply(h: Int, m: Int): Clock = {
    def loop(hours: Int, minutes: Int): (Int, Int) =
      if (hours > 23) loop(hours - 24, minutes)
      else if (minutes > 59) loop(hours + 1, minutes -60)
      else if (hours < 0) loop(hours + 24, minutes)
      else if (minutes < 0) loop(hours - 1, minutes + 60)
      else (hours, minutes)

    val (hours, minutes) = loop(h, m)
    new Clock(hours, minutes)
  }

  def unapply(clock: Clock): Option[(Int, Int)] = Some(clock.hours, clock.minutes)
}
