case class SpaceAge (seconds: Long) {
  val earthYear = 31557600d

  val onEarth = format(seconds / earthYear)
  val onMercury = format(seconds / (earthYear * 0.2408467))
  val onVenus = format(seconds / (earthYear * 0.61519726))
  val onMars = format(seconds / (earthYear * 1.8808158))
  val onJupiter = format(seconds / (earthYear * 11.862615))
  val onSaturn = format(seconds / (earthYear * 29.447498))
  val onUranus = format(seconds / (earthYear * 84.016846))
  val onNeptune = format(seconds / (earthYear * 164.79132))

  def format(n: Double): Double =
    BigDecimal(n).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
}
