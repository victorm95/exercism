object RunLengthEncoding {
  def encode(str: String): String =
    if (str.isEmpty) str
    else {
      val (first, rest) = str span (_ == str.head)
      (if (first.length > 1) first.length else "") +
        first.head.toString + encode(rest)
    }

  def decode(str: String): String =
    if (str.isEmpty) str
    else if (!str.head.isDigit) str.head.toString + decode(str.tail)
    else {
      val (num, text) = str span (_.isDigit)
      List.fill(num.toInt)(text.head).mkString + decode(text.tail)
    }
}
