object Pangrams {
  def isPangram(text: String): Boolean =
    text.toLowerCase.filter(_.isLetter).toSet.size == 26
}
