object Squares {
  private def sqrt(x: Int): Int =
    x * x

  def sumOfSquares(n: Int): Int =
    (1 to n).map(sqrt).sum

  def squareOfSums(n: Int): Int =
    sqrt((1 to n).sum)

  def difference(n: Int): Int =
    squareOfSums(n) - sumOfSquares(n)
}
