object Dna {
  val complements = Map(
    'G' -> 'C',
    'C' -> 'G',
    'T' -> 'A',
    'A' -> 'U'
  )

  def toRna(strand: String): Option[String] =
    if (strand forall (complements.contains)) Some(strand map complements)
    else None
}
