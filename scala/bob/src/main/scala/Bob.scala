class Bob {
  private def isYell(message: String): Boolean = {
    val letters = message filter (_.isLetter)
    letters.count(_.isUpper) > letters.count(_.isLower)
  }

  def hey(message: String): String =
    if (message.trim.isEmpty) "Fine. Be that way!"
    else if (isYell(message)) "Whoa, chill out!"
    else if (message.last == '?') "Sure."
    else "Whatever."
}
