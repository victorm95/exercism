class PhoneNumber(phone: String) {
  val number = {
    val numbers = phone filter (_.isDigit)
    if (numbers.length == 11 && numbers.head == '1') Some(numbers.tail)
    else if (numbers.length == 10) Some(numbers)
    else None
  }

  val areaCode = number map (_ take 3)

  val prettyPrint = number map (num => s"(${areaCode.get}) ${num drop 3 take 3}-${num drop 6}")
}
