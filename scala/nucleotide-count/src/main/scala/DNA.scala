class DNA(dna: String) {
  val symbols = List('A', 'C', 'G', 'T')
  val isValid = dna forall isDna

  private def isDna(symbol: Char): Boolean =
    symbols contains symbol

  def count(char: Char): Either[String, Int] =
    if (!isValid) Left(s"invalid nucleotide '${dna filterNot isDna}'")
    else if (isDna(char)) Right(dna count (_ == char))
    else Left(s"invalid nucleotide '$char'")

  def nucleotideCounts: Either[String, Map[Char, Int]] =
    if (isValid) Right(symbols.map(symbol => symbol -> dna.count(_ == symbol)).toMap)
    else Left(s"invalid nucleotide '${dna filterNot isDna}'")
}
