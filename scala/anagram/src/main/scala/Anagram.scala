class Anagram(text: String) {
  val upperCased = text.toUpperCase
  val anagram = reduce(upperCased.sorted)

  private def reduce(text: String): Map[Char, Int] =
    if (text.isEmpty) Map()
    else {
      val (first, rest) = text span (_ == text.head)
      Map(text.head -> first.length) ++ reduce(rest)
    }

  private def isAnagram(text: String): Boolean =
    reduce(text.toUpperCase.sorted) == anagram

  private def isDifferent(text: String): Boolean =
    text.toUpperCase != upperCased

  def matches(anagrams: Seq[String]): Seq[String] =
    anagrams.distinct
      .filter(isDifferent)
      .filter(isAnagram)
}
