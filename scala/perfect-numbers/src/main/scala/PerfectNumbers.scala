object NumberType extends Enumeration {
  val Perfect, Abundant, Deficient = Value
}

object PerfectNumbers {
  def classify(x: Int): Either[String, NumberType.Value] = {
    val sum = (1 until x).filter(x % _ == 0).sum

    if (x < 1) Left("Classification is only possible for natural numbers.")
    else if (sum == x) Right(NumberType.Perfect)
    else if (sum > x) Right(NumberType.Abundant)
    else Right(NumberType.Deficient)
  }
}
