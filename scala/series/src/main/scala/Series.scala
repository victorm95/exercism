object Series {
  type Serie = List[Int]

  def slices(n: Int, digits: String): List[Serie] = {
    def loop(numbers: String, acc: List[Serie]): List[Serie] =
      if (numbers.size < n) acc
      else loop(numbers.substring(1), acc ::: numbers.take(n).toList.map(_.toString.toInt) :: Nil)

    loop(digits, Nil)
  }
}
