(ns accumulate)

(defn accumulate [fn list]
  (if (empty? list)
    []
    (let [head (first list) tail (rest list)]
      (cons (fn head) (accumulate fn tail)))))
