module WordCount exposing (wordCount)

import Regex exposing (HowMany(All), regex)
import Dict exposing (Dict)


count : String -> Dict String Int -> Dict String Int
count word occurrences =
  if Dict.member word occurrences then
    Dict.update word (Maybe.map ((+) 1)) occurrences

  else
    Dict.insert word 1 occurrences


wordCount : String -> Dict String Int
wordCount text =
  let
    words =
      String.toLower text
        |> Regex.split All (regex "[^\\w]+")
        |> List.filter (not << String.isEmpty)
  in
    List.foldl count Dict.empty words
