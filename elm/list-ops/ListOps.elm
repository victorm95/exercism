module ListOps exposing (..)


length : List a -> Int
length list =
  let
    count _ total =
      total + 1
  in
    foldl count 0 list


reverse : List a -> List a
reverse list =
  let
    size =
      length list

    init =
      List.take (size - 1) list

    last =
      List.drop (size - 1) list
  in
    case last of
      [] ->
        []

      x :: _ ->
        x :: (reverse init)



map : (a -> b) -> List a -> List b
map fn list =
  case list of
    [] ->
      []

    x :: xs ->
      (fn x) :: (map fn xs)


filter : (a -> Bool) -> List a -> List a
filter fn list =
  case list of
    [] ->
      []

    x :: xs ->
      if fn x then
        x :: (filter fn xs)

      else
        filter fn xs


foldl : (a -> b -> b) -> b -> List a -> b
foldl fn initial list =
  case list of
    [] ->
      initial

    x :: xs ->
      foldl fn (fn x initial) xs


foldr : (a -> b -> b) -> b -> List a -> b
foldr fn initial list =
  foldl fn initial (reverse list)


append : List a -> List a -> List a
append list1 list2 =
  case list1 of
    [] ->
      list2

    x :: xs ->
      x :: (append xs list2)


concat : List (List a) -> List a
concat list =
  foldr append [] list
