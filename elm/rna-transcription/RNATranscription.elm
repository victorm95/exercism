module RNATranscription exposing (toRNA)

import Maybe exposing (Maybe(..), withDefault)


complement : Char -> Maybe Char
complement x =
  case x of
    'G' ->
      Just 'C'

    'C' ->
      Just 'G'

    'T' ->
      Just 'A'

    'A' ->
      Just 'U'

    _ ->
      Nothing


contains : Char -> Bool
contains x =
  case (complement x) of
    Just _ ->
      True

    Nothing ->
      False


toRNA : String -> Result Char String
toRNA strand =
  let
    chars =
      String.toList strand

    errs =
      List.filter (not << contains) chars
  in
    case (List.head errs) of
      Just x ->
        Err x

      Nothing ->
        Ok
          <| String.fromList
          <| List.map (complement >> withDefault '_') chars
