module Triangle exposing (..)


type Triangle
  = Equilateral
  | Isosceles
  | Scalene


triangleKind : Float -> Float -> Float -> Result String Triangle
triangleKind x y z =
  if x <= 0 || y <= 0 || z <= 0 then
    Err "Invalid lengths"

  else if (x + y < z) || (x + z < y) || (y + z < x) then
    Err "Violates inequality"

  else if x == y && x == z then
    Ok Equilateral

  else if x == y || x == z || y == z then
    Ok Isosceles

  else
    Ok Scalene
