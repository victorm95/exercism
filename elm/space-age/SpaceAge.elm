module SpaceAge exposing (ageOn, Planet(..))

type Planet
  = Mercury
  | Venus
  | Earth
  | Mars
  | Jupiter
  | Saturn
  | Uranus
  | Neptune


orbitalPeriod : Float -> Int -> Float
orbitalPeriod period seconds =
  (toFloat seconds) / (31557600 * period)


ageOn : Planet -> Int -> Float
ageOn planet =
  case planet of
    Mercury ->
      orbitalPeriod 0.2408467

    Venus ->
      orbitalPeriod 0.61519726

    Earth ->
      orbitalPeriod 1.0

    Mars ->
      orbitalPeriod 1.8808158

    Jupiter ->
      orbitalPeriod 11.862615

    Saturn ->
      orbitalPeriod 29.447498

    Uranus ->
      orbitalPeriod 84.016846

    Neptune ->
      orbitalPeriod 164.79132
