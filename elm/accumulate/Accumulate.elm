module Accumulate exposing (..)

accumulate : (a -> b) -> List a -> List b
accumulate fn data =
  case data of
    [] ->
      []
    x :: xs ->
      (fn x) :: (accumulate fn xs)
