module SumOfMultiples exposing (sumOfMultiples)

import Set

multiples : Int -> Int -> List Int
multiples limit num =
  List.range 1 (limit - 1)
    |> List.filter (\n -> n % num == 0)

sumOfMultiples : List Int -> Int -> Int
sumOfMultiples nums limit =
  List.concatMap (multiples limit) nums
    |> Set.fromList
    |> Set.toList
    |> List.sum
