module ScrabbleScore exposing (scoreWord)

import Dict exposing (Dict)
import Maybe exposing (withDefault)


value : Int -> Char -> (Char, Int)
value val char =
  (char, val)


scores : Dict Char Int
scores =
  let
    values =
      [ List.map (value 1) ['A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T']
      , List.map (value 2) ['D', 'G']
      , List.map (value 3) ['B', 'C', 'M', 'P']
      , List.map (value 4) ['F', 'H', 'V', 'W', 'Y']
      , List.map (value 5) ['K']
      , List.map (value 8) ['J', 'X']
      , List.map (value 10) ['Q', 'Z']
      ]
  in
    Dict.fromList (List.concat values)


scoreWord : String -> Int
scoreWord text =
  String.toUpper text
    |> String.toList
    |> List.map (flip Dict.get <| scores)
    |> List.map (withDefault 0)
    |> List.sum
