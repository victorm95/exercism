module Strain exposing (keep, discard)


keep : (a -> Bool) -> List a -> List a
keep fn list =
  case list of
    [] ->
      []

    x :: xs ->
      if fn x then
        x :: (keep fn xs)

      else
        keep fn xs


discard : (a -> Bool) -> List a -> List a
discard fn list =
  keep (not << fn) list
