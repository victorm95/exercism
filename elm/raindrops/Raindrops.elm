module Raindrops exposing (..)

raindrops : Int -> String
raindrops num =
  let
    result =
      (if num % 7 == 0 then "Plong" else "")
        |> (++) (if num % 5 == 0 then "Plang" else "")
        |> (++) (if num % 3 == 0 then "Pling" else "")
  in
    if String.isEmpty result then toString num else result
