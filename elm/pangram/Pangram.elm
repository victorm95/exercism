module Pangram exposing (isPangram)

import Regex exposing (regex, HowMany(All))
import Set

isPangram : String -> Bool
isPangram text =
  String.toLower text
    |> Regex.find All (regex "[a-z]")
    |> List.map .match
    |> Set.fromList
    |> Set.size
    |> (==) 26
